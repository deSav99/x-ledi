#include "dataledi.h"

#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QDebug>

#include "pin.h"
#include "itemledi.h"
#include "sceneledi.h"
#include "wire.h"



void DataLEdi::saveLE(QList<QGraphicsItem *> listItems)
{
    QString filename = "Save.le";
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly) or listItems.empty())
    {
        qWarning("Could not open file");
        return;
    }
    QTextStream out(&file);
    ItemLEdi* itemLe;
    Wire* wire;
    const QVector<Pin*>* pinsIn = nullptr;
    const QVector<Pin*>* pinsOut = nullptr;
    std::sort(listItems.begin(), listItems.end(), [](auto item1, auto item2 )
                                                      {return item1->type() > item2->type();});
    foreach(QGraphicsItem *item, listItems)
    {
        switch (item->type())
        {
        case ItemLEdi::Type:
            itemLe = qgraphicsitem_cast<ItemLEdi*>(item);
            pinsIn = itemLe->getInPins();
            pinsOut = itemLe->getOutPins();
            out << "ItemLEdi: " << Qt::endl << "\t"
                << "NameFirst: " << itemLe->getNameModule() << Qt::endl << "\t"
                << "NameLast: " << itemLe->getNameElement() << Qt::endl << "\t"
                << "Type: " << itemLe->typeItem() << Qt::endl << "\t"
                << "PinIn: " << itemLe->getPin().x() << Qt::endl << "\t"
                << "PinOut: " << itemLe->getPin().y() << Qt::endl << "\t"
                << "PosX: " << itemLe->scenePos().x() << Qt::endl << "\t"
                << "PosY: " << itemLe->scenePos().y() << Qt::endl << Qt::endl << "\t"
                << "Pins: " << Qt::endl;
            foreach(Pin* pin, *pinsIn)
            {
                    out << "\t\t" << "Input: " << pin->getNamePin() << Qt::endl;
            }
            foreach(Pin* pin, *pinsOut)
            {
                   out << "\t\t" << "Output: " << pin->getNamePin() << Qt::endl;
            }
            out << Qt::endl;

            break;
        case Wire::Type:
            wire = dynamic_cast<Wire*>(item);
              out << "Wire: " << Qt::endl << "\t"
                  << "NameElementFirst: " << wire->getPins().first->getNameElement() << Qt::endl << "\t"
                  << "NamePinFirst: " << wire->getPins().first->getNamePin() << Qt::endl << "\t"
                  << "NameElementEnd: " << wire->getPins().second->getNameElement() << Qt::endl << "\t"
                  << "NamePinEnd: " << wire->getPins().second->getNamePin() << Qt::endl << "\t"
                  << "LocationWire: " << wire->getlocationWire() << Qt::endl << Qt::endl;
            break;

        default: break;

        }
    }
}

void DataLEdi::loadLE(SceneLEdi *scene)
{
    ItemLEdi* currentItem = nullptr;
    ItemLEdi::TypeItem type{};
    QVector<Pin*> vecPinsIn{}, vecPinsOut{};
    QString nameModule{}, nameElement{};
    QString nameElementFirst{}, nameElementEnd{}, namePinFirst{}, namePinEnd{};
    bool locationWire{};
    QPoint pos{0,0};
    uint pinIn{0}, pinOut{0};

    QString filename = QFileDialog::getOpenFileName();
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning("Cannot open file for reading"); // если файл не найден, то выводим предупреждение и завершаем выполнение программы
        return;
      }
    QTextStream in(&file);
    QString str{};
    while(!in.atEnd())
    {
        in >> str;
        if (str == "ItemLEdi:")
        {
            in >> str;
            if(str == "NameFirst:")
            {
                in >> nameModule;
                in >> str;
            }
            if(str == "NameLast:")
            {
                in >> nameElement;
                in >> str;
            }
            if(str == "Type:")
            {
                in >> str;
                 type = static_cast<ItemLEdi::TypeItem>(str.toInt());
                 in >> str;
            }
            if(str == "PinIn:")
            {
                in >> str;
                pinIn = str.toUInt();
                in >> str;
            }
            if(str == "PinOut:")
            {
                in >> str;
                pinOut = str.toUInt();
                in >> str;
            }
            if(str == "PosX:")
            {
                in >> str;
                pos.rx() = str.toUInt();
                in >> str;
            }
            if(str == "PosY:")
            {
                in >> str;
                pos.ry() = str.toUInt();
                in >> str;
            }
            currentItem = new ItemLEdi{type, pinIn, pinOut, pos};
            if(str == "Pins:")
            {
                for(uint i = 0; i < pinIn + pinOut; ++i)
                {
                    in >> str;
                    if(str == "Input:")
                    {
                        in >> str;
                        vecPinsIn.push_back(new Pin{Pin::PinIn, str,
                                                    nameElement, currentItem});

                    }
                    else if(str == "Output:")
                    {
                        in >> str;
                        vecPinsOut.push_back(new Pin{Pin::PinOut, str,
                                                     nameElement, currentItem});

                    }
                }

            }
            currentItem->resetPins(vecPinsIn, vecPinsOut);
            scene->addItemToMap(currentItem);
            currentItem->resetItem(nameModule, nameElement);
            currentItem = nullptr;
            vecPinsIn.clear();
            vecPinsOut.clear();
        }
        else if(str == "Wire:")
        {
            in >> str;
            if(str == "NameElementFirst:")
            {
                in >> nameElementFirst;
                in >> str;
            }
            if(str == "NamePinFirst:")
            {
                in >> namePinFirst;
                in >> str;
            }
            if(str == "NameElementEnd:")
            {
                in >> nameElementEnd;
                in >> str;
            }
            if(str == "NamePinEnd:")
            {
                in >> namePinEnd;
                in >> str;
            }
            if(str == "LocationWire:")
            {
                in >> str;
                locationWire = str == "1" ? 1 : 0;
            }
            scene->addItem( new Wire{scene->findItem(nameElementFirst)->findPin(namePinFirst),
                                       scene->findItem(nameElementEnd)->findPin(namePinEnd),
                                       locationWire});
        }
    }
}
