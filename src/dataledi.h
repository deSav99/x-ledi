#ifndef DATALEDI_H
#define DATALEDI_H

#include <QGraphicsItem>
#include <QList>

class SceneLEdi;

class DataLEdi
{
public:
    DataLEdi() = default;
    void saveLE(QList<QGraphicsItem*> listItems);
    void loadLE(SceneLEdi* scene);
};

#endif // DATALEDI_H
