#include "itemledi.h"

#include "sceneledi.h"

#include <QPolygonF>
#include <QPixmap>
#include <QPainter>
#include <QApplication>
#include <QRandomGenerator>

ItemLEdi::ItemLEdi(TypeItem typeItem, uint countPinIn, uint countPinOut, QPoint pos, QGraphicsItem *parent)
    :QObject(), QGraphicsPolygonItem(parent), myTypeItem(typeItem), myCountPinIn(countPinIn), myCountPinOut(countPinOut)
{
    myCountPinIn = static_cast<uint>(QRandomGenerator::global()->bounded(1,5));;
    myCountPinOut = static_cast<uint>(QRandomGenerator::global()->bounded(1,5));;
    insertFlag = false;

    vectorInPins.reserve(myCountPinIn);
    vectorOutPins.reserve(myCountPinOut);
    switch(typeItem)
    {
    case Element:
        rectItem = QRectF{QPoint{-20,-40}, QPoint{20,40}};
        myPolygon = QPolygonF{rectItem};
        nameModule = new QGraphicsTextItem{"Module", this};
        break;
    case PortIn:
        myPolygon << QPoint{-25,-20} << QPoint{5,-20}
                  << QPoint{25,0} << QPoint{-25,0} << QPoint{-25,-20};
        nameModule = new QGraphicsTextItem("PortIn", this);
        break;
    case PortOut:
        myPolygon << QPoint{25,-20} << QPoint{-5,-20}
                  << QPoint{-25,0} << QPoint{25,0} << QPoint{25,-20};
        nameModule = new QGraphicsTextItem("PortOut", this);
        break;
    case Null:
        nameModule = nullptr;
        break;
    default:
        break;
    }
    setPolygon(myPolygon);
    nameElement = new TextItemLEdi{QString{"E%1"}.arg(countItem), this};
    connect(nameElement, &TextItemLEdi::sigNameChanged, this, &ItemLEdi::slotNameChanged);
    generatPin();
    ++countItem;

    setPos(pos);

    nameModule->setPos(boundingRect().topLeft() + QPointF{-10,-20});

    nameElement->setPos(boundingRect().bottomLeft());
    nameElement->setTextInteractionFlags(Qt::TextEditorInteraction);

    setFlags(ItemIsSelectable | ItemIsMovable |ItemSendsGeometryChanges);
}

size_t ItemLEdi::countItem{0};

void ItemLEdi::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    if(!insertFlag)
    {
        painter->setPen(QPen(Qt::green, 2, Qt::SolidLine,
                             Qt::RoundCap, Qt::MiterJoin));
    }
    else
    {
        painter->setPen(QPen(Qt::green, 1, Qt::SolidLine,
                             Qt::RoundCap, Qt::MiterJoin));
    }

    painter->drawPolygon(polygon());
    if (isSelected())
    {
        painter->setPen(QPen(QColor(0,255,0).lighter(), 3, Qt::SolidLine,
                             Qt::RoundCap, Qt::MiterJoin));
        painter->drawPolygon(polygon());
    }
}

QString ItemLEdi::stringTypeItem(TypeItem typeItem)
{
    switch(typeItem)
    {
    case Element: return "Element";
    case PortIn: return "PortIn";
    case PortOut: return "PortOut";
    default: return nullptr;
    }
}

QPointF ItemLEdi::binding(QPointF point)
{
    int xV = qRound(point.x()/20)*20;
    int yV = qRound(point.y()/20)*20;
    return QPoint{xV, yV};

}

QVariant ItemLEdi::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if(change == ItemPositionChange && scene())
    {
        QPointF newPos = value.toPointF();
        if(QApplication::mouseButtons() == Qt::LeftButton)
        {
            foreach(Pin* myPin, vectorInPins)
            {
                if(myPin->isConnected())
                {
                    foreach (Wire* myWire, myPin->getWires())
                    {
                        myWire->updatePos();
                    }
                }

            }

            foreach(Pin* myPin, vectorOutPins)
            {
                if(myPin->isConnected())
                {
                    foreach (Wire* myWire, myPin->getWires())
                    {
                        myWire->updatePos();
                    }
                }

            }

            return binding(newPos);
        }
        else
            return newPos;
    }
    else
        return QGraphicsItem::itemChange(change, value);
}

void ItemLEdi::resetPins(QVector<Pin*> vectorIn, QVector<Pin*> vectorOut)
{
    rectItem = QRectF{QPoint{-20,-40}, QPoint{20,40}};

    mapPins.clear();
    qDeleteAll(vectorInPins);
    qDeleteAll(vectorOutPins);
    vectorInPins = vectorIn;
    vectorOutPins = vectorOut;
    myCountPinIn = vectorInPins.size();
    myCountPinOut = vectorOutPins.size();
    generatPin();
    nameElement->setPos(boundingRect().bottomLeft());
    nameModule->setPos(boundingRect().topLeft() + QPointF{-10,-20});
}

void ItemLEdi::resetItem(QString nameMod = "", QString nameElem = "")
{
    if(nameMod != "")
    {
        nameModule->setPlainText(nameMod);
    }
    if(nameElem != "")
    {
        nameElement->setName(nameElem);
    }
}

void ItemLEdi::resize(const uint &countPin)
{
    QPointF newBottomRight{rectItem.bottomRight()
                + QPointF{0,static_cast<qreal>((countPin - 1)*40)}};
    rectItem = QRectF{rectItem.topLeft(), newBottomRight};
    myPolygon = QPolygonF{rectItem};
    setPolygon(myPolygon);
}

void ItemLEdi::pinAlignment(Pin::TypePin firstType, Pin::TypePin secondType, QPointF firstPos,
                            QPointF secondPos, uint firstCount, uint secondCount)
{
    Pin *pin;
    QString firstName{ firstType == Pin::PinIn ? "In%1" : "Out%1" };
    QString secondName{ secondType == Pin::PinIn ? "In%1" : "Out%1" };

    QVector<Pin*>& firstVec{ firstType == Pin::PinIn ? vectorInPins : vectorOutPins };
    QVector<Pin*>& secondVec{ secondType == Pin::PinIn ? vectorInPins : vectorOutPins };

    if(firstVec.empty() and secondVec.empty())
    {
        for(uint i = 0; i < firstCount; ++i)
        {
            pin = new Pin {firstType, firstName.arg(i), this->getNameElement(), this};
            firstVec.push_back(pin);
        }
        for(uint i = 0; i < secondCount; ++i)
        {
            pin = new Pin {secondType, secondName.arg(i), this->getNameElement(), this};
            secondVec.push_back(pin);
        }
    }
    resize(firstCount);
    foreach(Pin* myPin, firstVec)
    {
        mapPins[myPin->getNamePin()] = myPin;
        firstPos += QPointF{0, 40};
        myPin->setPos(firstPos);
    }

    foreach(Pin* myPin, secondVec)
    {
        mapPins[myPin->getNamePin()] = myPin;
        secondPos += (rectItem.bottomRight() - rectItem.topRight())/(secondCount + 1);
        myPin->setPos(binding(secondPos) - QPoint{10,0});
    }
}
void ItemLEdi::generatPin()
{
    Pin *pin = nullptr;
    QPointF PosPinIn{}, PosPinOut{};
    switch(myTypeItem)
    {

    case Element:
        PosPinIn = rectItem.topLeft() + QPointF{-10, 0};
        PosPinOut = rectItem.topRight() + QPointF{10,0};

        if(myCountPinIn > myCountPinOut)
        {
            pinAlignment(Pin::PinIn, Pin::PinOut, PosPinIn,
                         PosPinOut, myCountPinIn, myCountPinOut);
        }
        else
        {
            pinAlignment(Pin::PinOut, Pin::PinIn, PosPinOut,
                         PosPinIn, myCountPinOut, myCountPinIn);
        }
        break;
    case PortIn:
        pin = new Pin {Pin::PinOut,"In", this->getNameElement(), this};
        vectorInPins.push_back(pin);
        pin->setPos(rect().center() + QPointF{30, 0});
        break;
    case PortOut:
        pin = new Pin {Pin::PinIn, "Out", this->getNameElement(), this};
        vectorOutPins.push_back(pin);
        pin->setPos(rect().center() + QPointF{-30, 0});
        break;
    default:
        break;
    }
}

ItemLEdi::~ItemLEdi()
{
    --countItem;
    mapPins.clear();
    qDeleteAll(vectorInPins);
    qDeleteAll(vectorOutPins);

}
