#include "mainwindow.h"
#include "managerprogect.h"

#include <QApplication>
#include <QStyleFactory>
#include <QTextCodec>
#include <QSplashScreen>

QPalette darkStyle()
{
    QPalette darkPalette;
    darkPalette.setColor(QPalette::Window, QColor(46, 52, 57));
    darkPalette.setColor(QPalette::WindowText, Qt::white);
    darkPalette.setColor(QPalette::Base, QColor(30, 33, 36));
    darkPalette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
    darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
    darkPalette.setColor(QPalette::ToolTipText, Qt::white);
    darkPalette.setColor(QPalette::Text, Qt::white);
    darkPalette.setColor(QPalette::Button, QColor(46, 52, 57));
    darkPalette.setColor(QPalette::ButtonText, Qt::white);
    darkPalette.setColor(QPalette::BrightText, Qt::red);
    darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));
    darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
    darkPalette.setColor(QPalette::HighlightedText, Qt::black);

    return darkPalette;
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setWindowIcon(QIcon{":/images/X-LEdiIcon.png"});

    QSplashScreen splash{QPixmap{":/images/X-LEdi.png"}};
    splash.show();
    app.processEvents();

    app.setStyle(QStyleFactory::create("Fusion"));
    app.setPalette(darkStyle());

//    MainWindow window{};
//    window.show();
//    splash.finish(&window);
    ManagerWidget mW{};
    splash.finish(mW.getOpenWidget());

    return app.exec();
}
