#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    createAction();
    createMenu();
    createToolBox();
    createToolBar();
    tabWidget = nullptr;

    QWidget *widget = new QWidget{};
    QHBoxLayout *layout = new QHBoxLayout{};
    layout->addWidget(toolBox);

    createGraphics();

    layout->addWidget(tabWidget);

    resize(1500,900);
    widget->setLayout(layout);

    setCentralWidget(widget);
    connect(tabWidget, &QTabWidget::currentChanged, this, &MainWindow::currentChangedTab);
    connect(tabWidget, &QTabWidget::tabCloseRequested, this, &MainWindow::tabCloseRequested);
    connect(saveAction, &QAction::triggered, this, &MainWindow::saveDataLE);
    connect(openAction, &QAction::triggered, this, &MainWindow::loadDataLE);
}

void MainWindow::createMenu()
{
    QMenu *fileMenu = menuBar()->addMenu("&File");

    fileMenu->addAction(newFile);
    fileMenu->addAction(openAction);
    fileMenu->addAction(saveAction);
}

void MainWindow::createAction()
{
    newFile = new QAction{QIcon{":/images/newFile.png"},"newFile", this};
    newFile->setShortcut(tr("CTRL+T"));
    connect(newFile, &QAction::triggered, this, &MainWindow::newFileLE);

    openAction = new QAction{QIcon{":/images/Open.png"},"Open file", this};
    openAction->setShortcut(tr("CTRL+O"));

    saveAction = new QAction{QIcon{":/images/Save.png"},"Save open", this};
    saveAction->setShortcut(tr("CTRL+S"));

    deleteAction = new QAction{QIcon{":/images/Delete.png"}, "Delete item", this};
    deleteAction->setShortcut(tr("Delete"));
}

void MainWindow::createToolBar()
{
    pointerToolBar = addToolBar("");

    QToolButton *pointerButton = new QToolButton{};
    pointerButton->setIcon(QIcon{":/images/Mouse.png"});
    pointerButton->setCheckable(true);
    pointerButton->setChecked(true);

    QToolButton *wireButton = new QToolButton{};
    wireButton->setIcon(QIcon{":/images/Wire.png"});
    wireButton->setShortcut(tr("CTRL+W"));
    wireButton->setCheckable(true);

    pointerTypeGroup = new QButtonGroup{pointerToolBar};
    pointerTypeGroup->addButton(pointerButton, int(ViewLEdi:: MoveItem));
    pointerTypeGroup->addButton(wireButton, int(ViewLEdi::InsertWire));

    connect(pointerTypeGroup, QOverload<QAbstractButton *> ::of(&QButtonGroup::buttonClicked),
            this, &MainWindow::pointerGroupCliced);

    pointerToolBar->addWidget(pointerButton);
    pointerToolBar->addWidget(wireButton);
    pointerToolBar->addSeparator();
    pointerToolBar->addAction(deleteAction);

}

void MainWindow:: createGraphics()
{
    if (!tabWidget)
    {
        tabWidget = new QTabWidget{};
        tabWidget->setTabsClosable(true);
    }
    scene = new SceneLEdi{QRectF(0,0,3000,2000)};
    view = new ViewLEdi{scene};
    view->resize(scene->width(), scene->height());

    tabWidget->setCurrentIndex(tabWidget->addTab(view,"New scene"));
    tabList.push_back(view);

    connect(view, &ViewLEdi::insertItem, this, &MainWindow::insertItem);
    connect(view, &ViewLEdi::mouseMove, this, &MainWindow::createStatusBar);
    connect(deleteAction, &QAction::triggered, view, &ViewLEdi::deleteSelectItem);
}

void MainWindow::tabCloseRequested(int indexTab)
{
    ViewLEdi *removeView = tabList.at(indexTab);

    tabList.removeAt(indexTab);
    delete removeView;
//    if(tabList->empty())
//    {
//        startWidget = new QLabel{"Please Creat new File", this};
//        tabWidget->addTab(startWidget,"Start Progact");
//    }
//    else
}
void MainWindow::createStatusBar(QPointF mouseCoordinates)
{
     statusBar()->showMessage(QString("X:%1 Y:%2").arg(mouseCoordinates.x()).arg(mouseCoordinates.y()));
}

void MainWindow::currentChangedTab(int indexTab)
{
    if(tabList.size() < indexTab + 1) {return;}
    view = tabList.at(indexTab);
    scene = view->getSceneLE();
}

void MainWindow::createToolBox()
{
    toolBox = new QToolBox{};

    buttonGroup = new QButtonGroup{toolBox};
    connect(buttonGroup, QOverload<QAbstractButton *>::of(&QButtonGroup::buttonClicked),
            this, &MainWindow::buttonGroupClicked);

    QWidget *itemWidget = new QWidget{};
    QVBoxLayout *layout = new QVBoxLayout{};

    layout->addWidget(createCellWidget( "Element", ItemLEdi::Element));
    layout->addWidget(createCellWidget( "PortIn", ItemLEdi::PortIn));
    layout->addWidget(createCellWidget( "PortOut", ItemLEdi::PortOut));
    layout->addStretch(1);

    itemWidget->setLayout(layout);
    toolBox->setMinimumWidth(itemWidget->sizeHint().width());
    toolBox->setSizePolicy(QSizePolicy{QSizePolicy::Maximum, QSizePolicy::Ignored});
    toolBox->addItem(itemWidget, "Modules");

}

QWidget *MainWindow::createCellWidget(const QString &text, ItemLEdi::TypeItem type)
{
    QIcon icon{createImage(type)};

    QToolButton *button = new QToolButton{};
    button->setIcon(icon);
    button->setIconSize(QSize{50,50});

    buttonGroup->addButton(button, int(type));

    QVBoxLayout *layout = new QVBoxLayout{};
    layout->addWidget(button);
    layout->addWidget(new QLabel{text});

    layout->stretch(1);

    QWidget *widget = new QWidget{};
    widget->setLayout(layout);

    return widget;
}

QPixmap MainWindow::createImage(ItemLEdi::TypeItem type)
{
    QPixmap pixmap{250,250};
    pixmap.fill(Qt::transparent);
    QPainter painter{&pixmap};
    painter.setPen(QPen{Qt::green, 5});
    painter.translate(125,125);
    QPolygon myPolygon{};

    switch (type)
    {
    case ItemLEdi::Element:
        myPolygon << QPoint{-30, -75} << QPoint{-30, -40} << QPoint{-70, -40} << QPoint{-30, -40}
                  << QPoint{-30, 40} << QPoint{-70, 40} << QPoint{-30, 40} << QPoint{-30, 75}
                  << QPoint{30, 75} << QPoint{30, 0} << QPoint{70, 0} << QPoint{30, 0}
                  << QPoint{30, -75} << QPoint{-30, -75};
        break;
    case ItemLEdi::PortIn:
        myPolygon << QPoint{-65,-30} << QPoint{30,-30}
                  << QPoint{85,30} << QPoint{-65,30} << QPoint{-65,-30};
        break;
    case ItemLEdi::PortOut:
        myPolygon << QPoint{65,-30} << QPoint{-30,-30}
                  << QPoint{-85,30} << QPoint{65,30} << QPoint{65,-30};
        break;
    default:
        break;

    }
    painter.drawPolyline(myPolygon);

    return pixmap;
}

void MainWindow::buttonGroupClicked(QAbstractButton *button)
{
    if(view)
    {
        view->setItemType(ItemLEdi::TypeItem(buttonGroup->id(button)));
        view->setMode(ViewLEdi::InsertItem);
    }
}

void MainWindow::pointerGroupCliced()
{
    view->setMode(ViewLEdi::Mode(pointerTypeGroup->checkedId()));
}

void MainWindow::insertItem()
{
    pointerTypeGroup->button(int(ViewLEdi::MoveItem))->setChecked(true);
    view->setMode(ViewLEdi::Mode(pointerTypeGroup->checkedId()));

}

void MainWindow::newFileLE()
{
    createGraphics();
}

void MainWindow::saveDataLE()
{
    DataLEdi{}.saveLE(view->items());
}

void MainWindow::loadDataLE()
{
    createGraphics();
    DataLEdi{}.loadLE(view->getSceneLE());
}

MainWindow::~MainWindow()
{
    delete pointerTypeGroup;
    tabList.clear();
}

