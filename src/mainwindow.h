#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "itemledi.h"
#include "viewledi.h"
#include "sceneledi.h"

#include <QMainWindow>
#include <QButtonGroup>
#include <QToolButton>
#include <QToolBox>
#include <QAction>
#include <QTabWidget>
#include <QToolBar>
#include <QStatusBar>
#include <QMenuBar>
#include <QGraphicsView>
#include <QGridLayout>
#include <QLabel>
#include <QMap>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private slots:
    void newFileLE();
    void createStatusBar(QPointF mouseCoordinates);
    void buttonGroupClicked(QAbstractButton *button);
    void pointerGroupCliced();
    void insertItem();
    void currentChangedTab(int indexTab);
    void tabCloseRequested(int indexTab);
    void saveDataLE();
    void loadDataLE();


signals:
    void typeItem(ItemLEdi::TypeItem type);

private:
    void createMenu();
    void createAction();
    void createToolBox();
    void createToolBar();
    void createGraphics();

    QPixmap createImage(ItemLEdi::TypeItem type);
    QWidget *createCellWidget(const QString &text, ItemLEdi::TypeItem type);
    QToolBox *toolBox;
    QAction *newFile;
    QAction *deleteAction;
    QAction *saveAction;
    QAction *openAction;
    QToolBar *pointerToolBar;
    QTabWidget *tabWidget;
    QButtonGroup *buttonGroup;
    QButtonGroup *pointerTypeGroup;
    SceneLEdi *scene;
    ViewLEdi *view;
    QList <ViewLEdi*> tabList{};
};

#endif // MAINWINDOW_H
