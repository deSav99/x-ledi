#include "managerprogect.h"

#include<QBoxLayout>
#include <QHeaderView>
#include<QPushButton>
#include<QLineEdit>

OpenProjectWidget::OpenProjectWidget(ManagerWidget* managerWidget, QWidget *parent)
    : QDialog(parent), managerW(managerWidget)
{
    setWindowFlags(Qt::WindowCloseButtonHint);
    setMinimumSize(500,200);

    tableWidget = new QTableWidget{this};
    tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableWidget->setColumnCount(3);
    tableWidget->setHorizontalHeaderLabels(QStringList{"Name project", "Last modified", "Path"});
    tableWidget->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);

    QPushButton* createButton = new QPushButton{"Create"};
    connect(createButton, &QPushButton::clicked, managerWidget, &ManagerWidget::slotCreateWidget);
    QPushButton* addButton = new QPushButton{"Add Project"};
    QPushButton* openButton = new QPushButton{"Open"};
    openButton->setDefault(true);

    QVBoxLayout* vLayout = new QVBoxLayout{};
    vLayout->setSpacing(5);
    vLayout->addWidget(createButton,Qt::AlignTop);
    vLayout->addWidget(addButton,Qt::AlignTop);
    vLayout->addStretch();
    vLayout->addWidget(openButton,Qt::AlignBottom);

    QHBoxLayout* mainLayout = new QHBoxLayout{this};
    mainLayout->addWidget(tableWidget);
    mainLayout->addLayout(vLayout);
    setLayout(mainLayout);
}

CreateProjectWidget::CreateProjectWidget(ManagerWidget* managerWidget, QWidget *parent)
    : QDialog(parent), managerW(managerWidget)
{
    setWindowFlags(Qt::WindowCloseButtonHint);
    setMinimumSize(300,100);

    QLabel* nameLabel = new QLabel{"Name:"};
    nameLabel->setAlignment(Qt::AlignVCenter);
    QLabel* dirLabel = new QLabel{"Directory:"};
    nameLabel->setAlignment(Qt::AlignVCenter);

    QLineEdit* nameLineEdit = new QLineEdit{};
    QLineEdit* dirLineEdit = new QLineEdit{};

    QPushButton* dirButton = new QPushButton{"..."};
    QPushButton* createButton = new QPushButton{"Create"};
    createButton->setDefault(true);

    QGridLayout* mainGridLayout = new QGridLayout{this};
    mainGridLayout->addWidget(nameLabel, 0,0);
    mainGridLayout->addWidget(nameLineEdit, 0,1);
    mainGridLayout->addWidget(dirLabel, 1,0);
    mainGridLayout->addWidget(dirLineEdit, 1,1);
    mainGridLayout->addWidget(dirButton, 1,2);
    mainGridLayout->addWidget(createButton, 2,2, Qt::AlignBottom);
    setLayout(mainGridLayout);
}

ManagerWidget::ManagerWidget(QObject* parent) : QObject(parent)
{

    tupeWidget = OpenWidget;
    openWidget = nullptr;
    createWidget = nullptr;
    windowLEdi = nullptr;
    currentWidget(tupeWidget);
}

void ManagerWidget::currentWidget(TupeWidget tupe)
{
    switch (tupe)
    {
    case OpenWidget:
        if(openWidget) return;
        openWidget = new OpenProjectWidget{this};
        openWidget->show();
        break;
    case CreateWidget:
        if(createWidget) return;
        createWidget = new CreateProjectWidget{this};
        createWidget->show();
        break;
    case XLEdi:
        if(windowLEdi) return;
        delete openWidget;
        delete createWidget;
        openWidget = nullptr;
        createWidget = nullptr;
        windowLEdi = new MainWindow{};
        windowLEdi->show();
        break;
    }
}

ManagerWidget::~ManagerWidget()
{
    if(openWidget) delete openWidget;
    if(createWidget) delete createWidget;
    if(windowLEdi) delete windowLEdi;
}
