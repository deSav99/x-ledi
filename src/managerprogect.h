#ifndef MANAGERPROGECT_H
#define MANAGERPROGECT_H

#include <QDialog>
#include <QTableWidget>
#include <QButtonGroup>

#include "mainwindow.h"

class ManagerWidget;

class OpenProjectWidget : public QDialog
{
    Q_OBJECT
public:
    explicit OpenProjectWidget(ManagerWidget* managerWidget, QWidget *parent = nullptr);

private:
    QTableWidget* tableWidget;
    ManagerWidget* managerW;


signals:

};

class CreateProjectWidget : public QDialog
{
    Q_OBJECT
public:
    explicit CreateProjectWidget(ManagerWidget* managerWidget, QWidget *parent = nullptr);

private:
    ManagerWidget* managerW;


signals:

};

class ManagerWidget : public QObject
{
    Q_OBJECT

public:
    enum TupeWidget{OpenWidget, CreateWidget, XLEdi};
    ManagerWidget(QObject* parent = nullptr);
    ~ManagerWidget();
    void currentWidget(TupeWidget tupe);
    QWidget* getOpenWidget(){return openWidget;}

private:
    TupeWidget tupeWidget;
    OpenProjectWidget* openWidget;
    CreateProjectWidget* createWidget;
    MainWindow* windowLEdi;

public slots:
    void slotCreateWidget(){currentWidget(CreateWidget);}
};

#endif // MANAGERPROGECT_H
