#include "pin.h"

#include "wire.h"
#include <QPen>
#include <QDebug>

Pin::Pin(TypePin type, QString name, QString& nameElement, QGraphicsItem *parent)
    : QGraphicsLineItem{parent}, myTypePin{type}, namePin{name}, nameElement{nameElement}
{
    connected = false;

    this->setLine(-10, 0, 10, 0);
    this->setPen(QPen(QColor(0,255,0,100), 2, Qt::SolidLine,
                      Qt::RoundCap, Qt::MiterJoin));

    selectionRect = createSelectionRect();
    QGraphicsTextItem *textPin = new QGraphicsTextItem{namePin, this};
    textPin->setPos(boundingRect().topLeft() + QPointF{5,-5});
}

QRectF Pin::createSelectionRect()
{
    qreal extra = (pen().width() + 35) / 2.0;
    if(myTypePin == Pin::PinOut)
    {
        return QRectF(line().p1(),QSize(line().p2().x() - line().p1().x(),
                                        line().p2().y() - line().p1().y()))
                .adjusted(0, -extra, extra, extra);
    }
    else
    {
        return QRectF(line().p1(),QSize(line().p2().x() - line().p1().x(),
                                        line().p2().y() - line().p1().y()))
                .adjusted(-extra, -extra, 0, extra);

    }
}

void Pin::addWire(Wire *wire)
{
    listWires.push_back(wire);
    connected = true;
    setPen(QPen(Qt::green, 3, Qt::SolidLine ,
                Qt::RoundCap, Qt::RoundJoin));
}

void Pin::deleteWire(Wire *wire)
{
    listWires.removeOne(wire);
    if(listWires.empty())
    {
        connected = false;
        setPen(QPen(QColor(0,255,0,100), 2, Qt::SolidLine ,
                    Qt::RoundCap, Qt::RoundJoin));
    }
}

QPointF Pin::posPin()
{
    QPointF wireP = (myTypePin == Pin::PinIn) ? line().p1() : line().p2();
    return scenePos() + wireP;
}

QRectF Pin::boundingRect() const
{
    return selectionRect;
}

QPainterPath Pin::shape() const
{
    QPainterPath selectionPath;
    selectionPath.addRect(selectionRect);
    return selectionPath;
}

Pin::~Pin()
{
    if(!listWires.empty())
    {
        foreach(Wire *rmWire, listWires)
        {
            delete rmWire;
            rmWire = nullptr;
        }
    }
}
