#ifndef PIN_H
#define PIN_H

#include <QGraphicsLineItem>
class Wire;


class Pin: public QGraphicsLineItem
{
public:

    enum { Type = UserType + 18 };
    enum TypePin{ PinIn, PinOut };

    explicit Pin(TypePin type, QString name, QString& nameElement, QGraphicsItem *parent = nullptr);
    ~Pin();

    virtual int type() const override { return Type; }

    bool isConnected() const { return connected; }

    void addWire(Wire *wire);
    void deleteWire(Wire *wire);
    TypePin getTypePin() const { return myTypePin; };
    inline QString getNamePin() const { return namePin; };
    inline const QString& getNameElement() const { return nameElement; };
    void resetNameElement(QString& newName){nameElement = newName;}
    const QList <Wire*>& getWires() { return listWires; }
    QPointF posPin();

private:
    QList <Wire*> listWires{};
    bool connected;
    TypePin myTypePin;
    QRectF selectionRect;
    QString namePin;
    QString& nameElement;

    QRectF createSelectionRect();

protected:
    virtual QRectF boundingRect() const override;
    virtual QPainterPath shape() const override;
};

#endif // PIN_H
