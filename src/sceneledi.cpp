#include "sceneledi.h"

#include <QKeyEvent>
#include <QPainter>

SceneLEdi::SceneLEdi(const QRectF &sceneRect, QObject *parent)
    : QGraphicsScene(sceneRect, parent)
{
    lineWire1 = nullptr;
    lineWire2 = nullptr;
    firstPin = nullptr;
    endPin = nullptr;
    wire = nullptr;
    currentItem = nullptr;
    locationWire = true;
}

void SceneLEdi::drawBackground(QPainter *painter, const QRectF &rect)
{
    QPen pen{Qt::gray, 2, Qt::SolidLine,
                Qt::RoundCap, Qt::RoundJoin};
    painter->setPen(pen);
    for(int i = gridSize; i < rect.right(); i += gridSize)
        for(int j = gridSize; j < rect.bottom(); j += gridSize)
            painter->drawPoint(i, j);

}

QPoint SceneLEdi::binding(QPointF point)
{
    int xV = qRound(point.x()/gridSize)*gridSize;
    int yV = qRound(point.y()/gridSize)*gridSize;
    return QPoint{xV, yV};
}

void SceneLEdi::createItem(ItemLEdi::TypeItem type)
{
    if(currentItem)
    {
        if(currentItem->insertFlag)
        {
            type = currentItem->typeItem();
            currentItem->insertFlag = false;
            currentItem = nullptr;
        }
    }
    currentItem = new ItemLEdi(type);
    addItemToMap(currentItem);
    currentItem -> insertFlag = true;

    emit tranferItem(currentItem, nullptr);
}
void SceneLEdi::moveItem(QPointF mousePos)
{
    if(currentItem)
    {
        currentItem->setPos(binding(mousePos));
    }
}

void SceneLEdi::createWire(QPointF mousePos)
{
    QGraphicsItem *returningItem = this->itemAt(mousePos,QTransform());
    if(!returningItem) {return;}
    if(returningItem->type() != Pin::Type) {return;}

    if(!lineWire1)
    {
        firstPin = qgraphicsitem_cast<Pin*>(returningItem);
        firstPin ->setPen(QPen(Qt::green, 3, Qt::SolidLine ,
                                   Qt::RoundCap, Qt::RoundJoin));

        lineWire1 = new QGraphicsLineItem (QLineF(firstPin->posPin(),
                                                  firstPin->posPin()));
        lineWire2 = new QGraphicsLineItem (QLineF(firstPin->posPin(),
                                                  firstPin->posPin()), lineWire1);

        lineWire1->setPen(QPen(QColor(0,255,255,80), 2, Qt::SolidLine ,
                           Qt::RoundCap, Qt::RoundJoin));
        lineWire2->setPen(QPen(QColor(0,255,255,80), 2, Qt::SolidLine ,
                           Qt::RoundCap, Qt::RoundJoin));

        addItem(lineWire1);
        emit tranferItem(nullptr, lineWire2);
    }
    else
    {
        endPin = qgraphicsitem_cast<Pin*>(returningItem);
        if(firstPin != endPin)
        {
            wire = new Wire(firstPin,endPin,locationWire);
            addItem(wire);
            clearItem();
            firstPin = nullptr;
            endPin = nullptr;
            emit tranferItem(nullptr, nullptr);
        }
    }
}

void SceneLEdi::moveWire(QPointF mousePos)
{
    mousePos = binding(mousePos);
    if(lineWire1)
    {
        QPointF wire1P1 = lineWire1->line().p1();
        if(locationWire)
        {
            QLineF newWire1(wire1P1.x(), wire1P1.y(),
                            wire1P1.x(), mousePos.y());
            lineWire1->setLine(newWire1);

            QLineF newWire2(wire1P1.x(), mousePos.y(),
                            mousePos.x(),mousePos.y());
            lineWire2->setLine(newWire2);
        }
        else
        {
            QLineF newWire1(wire1P1.x(), wire1P1.y(),
                            mousePos.x(), wire1P1.y());
            lineWire1->setLine(newWire1);

            QLineF newWire2(mousePos.x(), wire1P1.y(),
                            mousePos.x(), mousePos.y());
            lineWire2->setLine(newWire2);
        }
    }
}

void SceneLEdi::slotNameChanged(QString oldName, QString newName)
{
    mapItems[newName] = mapItems.value(oldName);
    mapItems.remove(oldName);
}

void SceneLEdi::clearItem()
{
    if(currentItem)
    {
        currentItem->insertFlag = false;
        mapItems.remove(currentItem->getNameElement());
        removeItem(currentItem);
        delete currentItem;
        currentItem = nullptr;
    }
    else
        if(lineWire1)
        {
            if(!endPin)
            {
                firstPin->setPen(QPen(QColor(0,255,0,100), 2, Qt::SolidLine ,
                                             Qt::RoundCap, Qt::RoundJoin));
            }
            removeItem(lineWire1);
            delete lineWire1;
            lineWire1 = nullptr;
            lineWire2 = nullptr;
        }
}

void SceneLEdi::addItemToMap(QGraphicsItem* ptrItem)
{
    addItem(ptrItem);
    ItemLEdi* itemLE = nullptr;
    if(ptrItem->type() == ItemLEdi::Type)
    {
        itemLE = qgraphicsitem_cast<ItemLEdi*>(ptrItem);
        if(itemLE)
        {
            connect(itemLE, &ItemLEdi::sigNameChanged, this, &SceneLEdi::slotNameChanged);
            mapItems[itemLE->getNameElement()] = itemLE;
        }
    }
}
void SceneLEdi::deleteSelectItem()
{
    foreach(auto item, mapItems)
    {
        if(item->isSelected()) mapItems.remove(item->getNameElement());
    }
    qDeleteAll(selectedItems());
}

SceneLEdi::~SceneLEdi()
{
    qDeleteAll(mapItems);
    if(items().empty()) qDebug() << "Scene Clear!";
}
