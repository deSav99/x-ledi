#include "textitemledi.h"

#include <QKeyEvent>

TextItemLEdi::TextItemLEdi(const QString &text, QGraphicsItem *parent )
    : QGraphicsTextItem(text, parent), nameElement{text}
{
        setTextInteractionFlags(Qt::TextEditable);
        setTabChangesFocus(true);
}

void TextItemLEdi::keyPressEvent(QKeyEvent* e)
{
    switch (e->key())
    {
    case Qt::Key_Space:
        break;
    case Qt::Key_Control:
        clearFocus();
        break;
    case Qt::Key_Return:
        clearFocus();
        break;
    default:
        QGraphicsTextItem::keyPressEvent(e);
        break;
    }
}

void TextItemLEdi::focusOutEvent(QFocusEvent *event)
{
    emit sigNameChanged(nameElement, toPlainText());
    nameElement = toPlainText();
    QGraphicsTextItem::focusOutEvent(event);
}
