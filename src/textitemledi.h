#ifndef TEXTITEMLEDI_H
#define TEXTITEMLEDI_H

#include <QGraphicsTextItem>
#include <QDebug>

class TextItemLEdi : public QGraphicsTextItem
{
    Q_OBJECT
public:
    TextItemLEdi(const QString &text, QGraphicsItem *parent = nullptr);
    inline QString& getString() const {return refNameElement;}
    inline void setName(QString& str) {emit sigNameChanged(nameElement, str);
                                       setPlainText(str);}
private:
    QString nameElement;
    QString& refNameElement{nameElement};

protected:
     virtual void keyPressEvent(QKeyEvent* e) override;
    virtual void focusOutEvent(QFocusEvent *event) override;

signals:
    void sigNameChanged(QString& oldName, QString newName);

};

#endif // TEXTITEMLEDI_H
