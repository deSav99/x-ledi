#include "viewledi.h"

#include <QApplication>

ViewLEdi::ViewLEdi(SceneLEdi *scene, QWidget *parent) : QGraphicsView(scene, parent), sceneLE(scene)
{
    editor = nullptr;
    currentItem = nullptr;
    currentWire = nullptr;
    selectEditorItem = nullptr;
    setMouseTracking(true);
    myMode = MoveItem;
    setDragMode(QGraphicsView::RubberBandDrag);
    connect(sceneLE, &SceneLEdi::tranferItem, this, &ViewLEdi::transferItem);
}

void ViewLEdi::transferItem(ItemLEdi *item, QGraphicsLineItem *line)
{
    if(item)
    {
        currentItem = item;
    }
        currentWire = line;
}

 void ViewLEdi:: mouseDoubleClickEvent(QMouseEvent *mouseEvent)
 {
     if(mouseEvent->button() == Qt::LeftButton and myMode == MoveItem)
     {
         selectEditorItem = itemAt(mouseEvent->pos());
         if(selectEditorItem && !editor && selectEditorItem->type() == ItemLEdi::Type)
         {
             editor = new CodeEditor(this);
             connect(editor,&CodeEditor::closeWindow , this, &ViewLEdi::closeEditor);
             editor->show();
         }
     }
 }


 void ViewLEdi::mousePressEvent(QMouseEvent *mouseEvent)
 {
     if(mouseEvent->button() == Qt::RightButton)
     {
         if(!currentItem and !currentWire)
         {
             emit insertItem();
             return;
         }
         if(currentItem)
         {
             currentItem = nullptr;
             emit insertItem();
         }
         if(currentWire)
         {
             currentWire = nullptr;
         }
        sceneLE->clearItem();
         return;
     }
     switch(myMode)
     {
     case InsertItem:
         sceneLE->createItem();
         break;

     case InsertWire:
         sceneLE->createWire(mapToScene(mouseEvent->pos()));
            break;
         case MoveItem:

           break;
     default:
         break;
     }
     QGraphicsView::mousePressEvent(mouseEvent);
 }

 void ViewLEdi::mouseMoveEvent(QMouseEvent *mouseEvent)
 {
     movePos = mapToScene(mouseEvent->pos());
     switch (myMode) {
     case MoveItem:
         QGraphicsView::mouseMoveEvent(mouseEvent);

           break;
     case InsertItem:
         if(currentItem)
         {
             sceneLE->moveItem(movePos);
             ensureVisible(currentItem);
         }
         break;
     case InsertWire:
         if(currentWire)
         {
             sceneLE->moveWire(movePos);
             this->ensureVisible(currentWire);
         }

         break;
     default:
         break;
     }
     emit mouseMove(mapToScene(mouseEvent->pos()));
 }

 void ViewLEdi::keyPressEvent(QKeyEvent *event)
 {
     switch (event->key())
     {
     case Qt::Key_A:
     {
         if(QApplication::keyboardModifiers() & Qt::ControlModifier)
         {
             foreach (QGraphicsItem *item, items())
             {
                 item->setSelected(true);
             }
         }
         break;
     }
     default:
         break;
     }
     QGraphicsView::keyPressEvent(event);
 }

 void ViewLEdi::wheelEvent(QWheelEvent *event)
 {
     switch (myMode)
     {
     case MoveItem:
         QGraphicsView::wheelEvent(event);

           break;
     case InsertItem:

         break;
     case InsertWire:
         if(!event->angleDelta().isNull())
         {
             sceneLE->setLocationWire();
             sceneLE->moveWire(movePos);
         }

         break;
     default:
         break;
     }
 }
 void ViewLEdi::closeEditor()
 {
     selectEditorItem = nullptr;
     delete editor;
     editor = nullptr;
 }

 ViewLEdi::~ViewLEdi()
 {
     qDebug() << "View clear!";
     delete sceneLE;
     sceneLE = nullptr;

     if(editor)
     {
         delete editor;
         editor = nullptr;
     }
 }

