#ifndef VIEWLEDI_H
#define VIEWLEDI_H

#include "codeeditor.h"
#include "sceneledi.h"
#include "dataledi.h"

#include <QGraphicsView>

class ViewLEdi : public QGraphicsView
{
    Q_OBJECT
public:
    explicit ViewLEdi(SceneLEdi *scene, QWidget *parent = nullptr);
    ~ViewLEdi();

    SceneLEdi* getSceneLE() {return sceneLE;};


    enum Mode{MoveItem, InsertItem, InsertWire};
    Mode myMode;
    void setItemType(ItemLEdi::TypeItem type) { sceneLE->createItem(type);};

private:

    SceneLEdi *sceneLE;
    CodeEditor *editor;
    QGraphicsItem *selectEditorItem;
    ItemLEdi *currentItem;
    QGraphicsLineItem *currentWire;
    QPointF movePos{};
protected:
    virtual void mousePressEvent(QMouseEvent *mouseEvent) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *mouseEvent) override;
    virtual void mouseMoveEvent(QMouseEvent *mouseEvent) override;
//    void mouseReleaseEvent(QMouseEvent *mouseEvent);
    virtual void keyPressEvent(QKeyEvent *event) override;
    virtual void wheelEvent(QWheelEvent *event) override;

public slots:
    void closeEditor();
    void transferItem(ItemLEdi *item , QGraphicsLineItem *line);
    void setMode(Mode mode) {myMode = mode;}
    void deleteSelectItem(){sceneLE->deleteSelectItem();}

signals:
    void mouseMove(QPointF mouseCoordinates);
    void insertItem();
};

#endif // VIEWLEDI_H
