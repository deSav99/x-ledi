#include "wire.h"

#include "pin.h"
#include "sceneledi.h"

#include <QPainter>

Wire::Wire(Pin *firstPin, Pin *endPin,
           bool locationWire, QGraphicsItem *parent)
    : QGraphicsPathItem(parent), myFirstPin(firstPin), myEndPin(endPin), locationWire(locationWire)
{
    firstPin->setPen(QPen(Qt::green, 3, Qt::SolidLine ,
                        Qt::RoundCap, Qt::MiterJoin));
    endPin->setPen(QPen(Qt::green, 3, Qt::SolidLine ,
                        Qt::RoundCap, Qt::MiterJoin));
    setFlag(QGraphicsItem ::ItemIsSelectable);

    updatePos();

    firstPin->addWire(this);
    endPin->addWire(this);
    //setFlags(QGraphicsItem ::ItemIsSelectable | QGraphicsItem::ItemSendsGeometryChanges | QGraphicsItem::ItemIsMovable);
}

    void Wire::createSelectionPolygon()
{
        selectionPolygon.clear();
        selectionPolygon << myFirstPin->posPin() << shoulder + QPoint{10,-10}
                         << myEndPin->posPin() << shoulder + QPoint{-10,10}
                         << myFirstPin->posPin();

}


QRectF Wire::boundingRect() const
{
    return selectionPolygon.boundingRect();
}


QPainterPath Wire::shape() const
{
    QPainterPath path;
    path.addPolygon(selectionPolygon);
    return path;
}

void Wire::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    painter->setPen(QPen(Qt::cyan, 2, Qt::SolidLine ,
                          Qt::RoundCap, Qt::MiterJoin));
    painter->drawPath(path());
    if (isSelected())
    {
        painter->setPen(QPen(QColor(0,200,255), 2, Qt::SolidLine ,
                             Qt::RoundCap, Qt::MiterJoin));
        painter->drawPath(path());
    }
}

void Wire::updatePos()
{
    QPainterPath myPath (myFirstPin->posPin());
    if(locationWire)
    {
        shoulder = QPointF{myFirstPin->posPin().x(), myEndPin->posPin().y()};
        myPath.lineTo(shoulder);
    }
    else
    {
        shoulder = QPointF{myEndPin->posPin().x(), myFirstPin->posPin().y()};
        myPath.lineTo(shoulder);
    }
    myPath.lineTo(myEndPin->posPin());
    createSelectionPolygon();
    this->setPath(myPath);
    update();
}

Wire:: ~Wire()
{
    myFirstPin->deleteWire(this);
    myEndPin->deleteWire(this);
}

