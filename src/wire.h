#ifndef WIRE_H
#define WIRE_H

#include <QGraphicsPathItem>
#include <QPair>

class Pin;

class Wire : public QGraphicsPathItem
{
public:
    enum {Type = UserType + 4};

    Wire(Pin *firstPin, Pin *endPin,
         bool locationWire, QGraphicsItem *parent = nullptr);

    ~Wire();

    virtual int type() const override { return Type; }
    inline const QPair<Pin*, Pin*> getPins() const{return QPair{myFirstPin, myEndPin};}
    inline const bool& getlocationWire() const{return locationWire;}

    void updatePos();

protected:

    virtual QPainterPath shape() const override;
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    Pin *myFirstPin;
    Pin *myEndPin;
    QPointF shoulder{};
    bool locationWire{};
    QPolygonF selectionPolygon{};

    void createSelectionPolygon();
};

#endif // WIRE_H
